package main

import (
	"os"
	"time"

	"github.com/BurntSushi/toml"
)

type duration struct {
	time.Duration
}

func (d *duration) UnmarshalText(text []byte) error {
	var err error
	d.Duration, err = time.ParseDuration(string(text))
	return err
}

const (
	defaultCacheTTL         time.Duration = 5 * time.Minute
	defaultCacheCleanPeriod time.Duration = 10 * time.Minute
	defaultCacheStore       string        = "./geo-cache"

	defaultIPStackRPSLimit uint64 = uint64(100)
	defaultNekudoRPSLimit  uint64 = uint64(1000)
)

type config struct {
	CacheTTL         duration
	CacheCleanPeriod duration
	CacheStore       string

	IPStackRPSLimit uint64
	NekudoRPSLimit  uint64
}

func readConfig(fname string) (*config, error) {
	_, err := os.Stat(fname)
	if err != nil {
		return nil, err
	}

	conf := config{}
	if _, err := toml.DecodeFile(fname, &conf); err != nil {
		return nil, err
	}

	return &conf, nil
}

func newDefaultConfig() *config {
	conf := config{}

	conf.CacheTTL.Duration = defaultCacheTTL
	conf.CacheCleanPeriod.Duration = defaultCacheCleanPeriod
	conf.CacheStore = defaultCacheStore

	conf.IPStackRPSLimit = defaultIPStackRPSLimit
	conf.NekudoRPSLimit = defaultNekudoRPSLimit

	return &conf
}
