package main

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestCounter(t *testing.T) {
	c := newRequestCounter()
	c.addRequest()
	time.Sleep(1 * time.Second)
	assert.Equal(t, c.getRPS(), uint64(0), "Test requestCounter failed")
	assert.Equal(t, c.getTotal(), uint64(1), "Test requestCounter failed")
	assert.Equal(t, c.getRPM(), uint64(1), "Test requestCounter failed")
	assert.Equal(t, c.getRPH(), uint64(1), "Test requestCounter failed")
	time.Sleep(10 * time.Second)
	assert.Equal(t, c.getRPS(), uint64(0), "Test requestCounter failed")
	c.addRequest()
	c.addRequest()
	assert.Equal(t, c.getRPM(), uint64(3), "Test requestCounter failed")
	time.Sleep(52 * time.Second)
	assert.Equal(t, c.getRPM(), uint64(2), "Test requestCounter failed")
	assert.Equal(t, c.getTotal(), uint64(3), "Test requestCounter failed")
	assert.Equal(t, c.getRPH(), uint64(3), "Test requestCounter failed")
	assert.Equal(t, c.getRPS(), uint64(0), "Test requestCounter failed")
}
