package main

// GeoIPProvider is abstract interface for GEO IP provider
type GeoIPProvider interface {
	GetRPS() uint64
	MaxRPS() uint64
	GetCountryByIP(ip string) string
	Close()
}

type geoIPProviderType int

const (
	localGeoIP geoIPProviderType = 1 << iota
	ipStack
	nekudo
)

func newGeoIPProvider(t geoIPProviderType, maxRPS uint64) GeoIPProvider {
	switch t {
	case localGeoIP:
		return newLocalGeoIPProvider()
	case ipStack:
		return newIPStackGeoIPProvider(maxRPS)
	case nekudo:
		return newNekudoGeoIPProvider(maxRPS)
	}
	return nil
}
