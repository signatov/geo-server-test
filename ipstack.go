package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

const (
	ipStackAccessKey   = "put you access key here"
	ipStackProviderURL = "http://api.ipstack.com/%s?language=en&access_key=%s"
)

type ipstackGeoIPProvider struct {
	counter  *requestCounter
	rpsLimit uint64
}

func newIPStackGeoIPProvider(maxRPS uint64) *ipstackGeoIPProvider {
	return &ipstackGeoIPProvider{counter: newRequestCounter(), rpsLimit: maxRPS}
}

func (provider *ipstackGeoIPProvider) GetRPS() uint64 {
	return provider.counter.getRPS()
}

func (provider *ipstackGeoIPProvider) MaxRPS() uint64 {
	return provider.rpsLimit
}

func (provider *ipstackGeoIPProvider) Close() {
	provider.counter.flush()
}

func (provider *ipstackGeoIPProvider) GetCountryByIP(ip string) string {
	fmt.Println("Get country name using IPStack provider")

	provider.counter.addRequest()

	key := os.Getenv("IPSTACK_APIKEY")
	if key == "" {
		key = ipStackAccessKey
	}
	resp, err := http.Get(fmt.Sprintf(ipStackProviderURL, ip, key))
	if err != nil {
		return ""
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return ""
	}

	var data map[string]interface{}
	if err := json.Unmarshal(body, &data); err != nil {
		return ""
	}

	fmt.Println(data)

	if data["country_name"] != nil {
		switch data["country_name"].(type) {
		case string:
			return data["country_name"].(string)
		}
	}

	return ""
}
