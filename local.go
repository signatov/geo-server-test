package main

import (
	"fmt"
	"log"
	"net"
	"os"

	geoip2 "github.com/oschwald/geoip2-golang"
)

type localGeoIPProvider struct {
	db *geoip2.Reader
}

func newLocalGeoIPProvider() *localGeoIPProvider {
	provider := localGeoIPProvider{}
	err := provider.init()
	if err != nil {
		return nil
	}
	return &provider
}

func (provider *localGeoIPProvider) init() error {
	fname := os.Getenv("GEO_DB")
	if fname == "" {
		fname = "GeoLite2-City.mmdb"
	}
	db, err := geoip2.Open(fname)
	if err != nil {
		log.Println(err)
		return fmt.Errorf("Cannot open maxdb %s", fname)
	}

	provider.db = db
	return nil
}

func (provider *localGeoIPProvider) GetRPS() uint64 {
	// There are no limits for local GEO IP provider so we don't have to count rps
	return 0
}

func (provider *localGeoIPProvider) MaxRPS() uint64 {
	// There are no limits for local GEO IP provider
	return ^uint64(0)
}

func (provider *localGeoIPProvider) Close() {
	provider.db.Close()
}

func (provider *localGeoIPProvider) GetCountryByIP(ip string) string {
	record, err := provider.db.City(net.ParseIP(ip))
	if err != nil {
		return ""
	}

	return record.Country.Names["en"]
}
