package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

const nekudoProviderURL = "http://geoip.nekudo.com/api/%v/en/short"

type nekudoGeoIPProvider struct {
	counter  *requestCounter
	rpsLimit uint64
}

func newNekudoGeoIPProvider(maxRPS uint64) *nekudoGeoIPProvider {
	return &nekudoGeoIPProvider{counter: newRequestCounter(), rpsLimit: maxRPS}
}

func (provider *nekudoGeoIPProvider) GetRPS() uint64 {
	return provider.counter.getRPS()
}

func (provider *nekudoGeoIPProvider) MaxRPS() uint64 {
	return provider.rpsLimit
}

func (provider *nekudoGeoIPProvider) Close() {
	provider.counter.flush()
}

func (provider *nekudoGeoIPProvider) GetCountryByIP(ip string) string {
	fmt.Println("Get country name using Nekudo provider")

	provider.counter.addRequest()

	resp, err := http.Get(fmt.Sprintf(nekudoProviderURL, ip))
	if err != nil {
		return ""
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return ""
	}

	var data map[string]interface{}
	if err := json.Unmarshal(body, &data); err != nil {
		return ""
	}

	fmt.Println(data)

	if data["country"] != nil {
		return (data["country"].(map[string]interface{}))["name"].(string)
	}

	return ""
}
