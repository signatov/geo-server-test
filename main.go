package main

import (
	"context"
	"encoding/gob"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"sync/atomic"
	"time"

	"github.com/gorilla/mux"
	cache "github.com/patrickmn/go-cache"
)

// Key is own type for work with context
type Key int

const (
	requestIDKey Key = 0
)

var (
	healthy   int32
	cfg       *config
	ipCache   *cache.Cache
	providers []GeoIPProvider
)

func nextRequestID() string {
	return fmt.Sprintf("%d", time.Now().UnixNano())
}

func nextGeoIPProvider() GeoIPProvider {
	for _, prov := range providers {
		if prov.GetRPS() < prov.MaxRPS() {
			return prov
		}
	}

	return nil
}

func countryFromIP(provider GeoIPProvider, ip string) string {
	if provider == nil {
		return ""
	}
	return provider.GetCountryByIP(ip)
}

func ipFromRequest(request *http.Request) string {
	xReal := request.Header.Get("X-Real-Ip")
	ip := request.Header.Get("X-Forwarded-For")
	if ip == "" && xReal == "" {
		if strings.ContainsRune(request.RemoteAddr, ':') {
			ip, _, _ = net.SplitHostPort(request.RemoteAddr)
		} else {
			ip = request.RemoteAddr
		}
	} else if ip != "" {
		for _, address := range strings.Split(ip, ",") {
			address := strings.TrimSpace(address)
			if net.ParseIP(address) != nil {
				// TODO: check private address
				ip = address
			}
		}
	} else {
		ip = xReal
	}

	return ip
}

func logHandler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		log.Printf("Handling %q\n", request.RequestURI)
		next.ServeHTTP(writer, request)
	})
}

func tracingHandler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		requestID := request.Header.Get("X-Request-Id")
		if requestID == "" {
			requestID = nextRequestID()
		}
		context := context.WithValue(request.Context(), requestIDKey, requestID)
		writer.Header().Set("X-Request-Id", requestID)
		next.ServeHTTP(writer, request.WithContext(context))
	})
}

func healthCheckHandler(writer http.ResponseWriter, request *http.Request) {
	if atomic.LoadInt32(&healthy) == 1 {
		writer.WriteHeader(http.StatusNoContent)
	} else {
		writer.WriteHeader(http.StatusServiceUnavailable)
	}
}

func ipHandler(writer http.ResponseWriter, request *http.Request) {
	type IPResponse struct {
		IP string `json:"ip"`
	}

	writer.Header().Set("Content-Type", "application/json")
	response := IPResponse{IP: ipFromRequest(request)}
	json.NewEncoder(writer).Encode(&response)
}

func countryHandler(writer http.ResponseWriter, request *http.Request) {
	type CountryResponse struct {
		Country string `json:"country"`
	}

	ip := ipFromRequest(request)
	item, found := ipCache.Get(ip)
	country := ""
	if found {
		country = item.(string)
	} else {
		country = countryFromIP(nextGeoIPProvider(), ip)
	}

	if country != "" {
		ipCache.Set(ip, country, cache.DefaultExpiration)
	}

	writer.Header().Set("Content-Type", "application/json")
	response := CountryResponse{Country: country}
	json.NewEncoder(writer).Encode(&response)
}

func initGeoProviders(cfg *config) []GeoIPProvider {
	prov := []GeoIPProvider{}
	prov = append(prov, newGeoIPProvider(ipStack, cfg.IPStackRPSLimit))
	prov = append(prov, newGeoIPProvider(nekudo, cfg.NekudoRPSLimit))
	local := newGeoIPProvider(localGeoIP, 0)
	if local != nil {
		prov = append(prov, local)
	}
	return prov
}

func loadGob(fname string, object interface{}) error {
	fd, err := os.Create(fname)
	if err != nil {
		return err
	}
	encoder := gob.NewDecoder(fd)
	err = encoder.Decode(object)
	fd.Close()
	return err
}

func saveGob(fname string, object interface{}) error {
	fd, err := os.Create(fname)
	if err != nil {
		return err
	}
	encoder := gob.NewEncoder(fd)
	err = encoder.Encode(object)
	fd.Close()
	return err
}

func main() {
	var listenAddress string
	var configFile string

	flag.StringVar(&listenAddress, "listen-addr", ":8384", "server listen address")
	flag.StringVar(&configFile, "config-file", "./sample.toml", "config file path")
	flag.Parse()

	var err error
	cfg, err = readConfig(configFile)
	if err != nil {
		log.Printf("Couldn't read config file %s: %v\n", configFile, err)
		log.Println("Will use default config")
		cfg = newDefaultConfig()
	}

	router := mux.NewRouter()
	router.HandleFunc("/health", healthCheckHandler).Methods("GET")
	router.HandleFunc("/ip", ipHandler).Methods("GET")
	router.HandleFunc("/country", countryHandler).Methods("GET")
	router.Use(tracingHandler)
	router.Use(logHandler)

	log.Printf("Starting server at %q...\n", listenAddress)

	if _, err := os.Stat(cfg.CacheStore); err != nil {
		items := make(map[string]cache.Item)
		if err := loadGob(cfg.CacheStore, items); err != nil {
			ipCache = cache.New(cfg.CacheTTL.Duration, cfg.CacheCleanPeriod.Duration)
		} else {
			ipCache = cache.NewFrom(cfg.CacheTTL.Duration, cfg.CacheCleanPeriod.Duration, items)
		}
	} else {
		ipCache = cache.New(cfg.CacheTTL.Duration, cfg.CacheCleanPeriod.Duration)
	}

	providers = initGeoProviders(cfg)

	server := &http.Server{
		Addr:         listenAddress,
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
		IdleTimeout:  30 * time.Second,
		Handler:      router,
	}

	done := make(chan bool)
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)

	go func() {
		<-quit

		log.Println("Server is shutting down...")
		atomic.StoreInt32(&healthy, 0)

		context, cancel := context.WithTimeout(context.Background(), 30*time.Second)
		defer cancel()

		server.SetKeepAlivesEnabled(false)
		if err := server.Shutdown(context); err != nil {
			log.Fatalf("Could not gracefully shutdown the server: %v\n", err)
		}

		if err := saveGob(cfg.CacheStore, ipCache.Items()); err != nil {
			os.Remove(cfg.CacheStore)
		}

		for _, prov := range providers {
			prov.Close()
		}

		close(done)
	}()

	atomic.StoreInt32(&healthy, 1)

	if err := server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		log.Fatalf("Couldn't listen on %s: %v\n", listenAddress, err)
	}

	<-done
	log.Println("Server stopped")
}
