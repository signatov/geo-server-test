package main

import (
	"sync"
	"sync/atomic"
	"time"
)

const (
	hourNS   = 36000000000000
	minuteNS = 60000000000
	secondNS = 1000000000
)

type request struct {
	time int64
	next *request
	prev *request
}

type requestCounter struct {
	head      *request
	tail      *request
	minute    *request
	second    *request
	mutex     sync.RWMutex
	perSecond uint64
	perMinute uint64
	perHour   uint64
	total     uint64
}

func newRequestCounter() *requestCounter {
	return &requestCounter{
		head: nil, tail: nil, minute: nil, second: nil,
		perSecond: 0, perMinute: 0, perHour: 0, total: 0}
}

func (c *requestCounter) addRequest() {
	c.incCounters(&c.perSecond, &c.perMinute, &c.perHour, &c.total)

	now := time.Now().UnixNano()

	r := &request{time: now, next: nil, prev: nil}
	c.mutex.Lock()
	if c.head == nil {
		c.tail = r
		c.head = c.tail
		c.minute = c.tail
		c.second = c.tail
	} else {
		r.next = c.head
		c.head.prev = r
		c.head = r
		if c.minute == nil {
			c.minute = c.head
		}
		if c.second == nil {
			c.second = c.head
		}
	}
	c.mutex.Unlock()

	c.refresh(now)
}

func (c *requestCounter) refresh(now int64) {
	c.mutex.Lock()
	c.checkExpires(now, hourNS, &c.perHour, &c.perMinute, &c.perSecond)
	c.softCheckExpires(now, minuteNS, &c.minute, &c.perMinute)
	c.softCheckExpires(now, secondNS, &c.second, &c.perSecond)
	c.mutex.Unlock()
}

func (c *requestCounter) softCheckExpires(now int64, expireDelta int64, p **request, counter *uint64) {
	if *p != nil {
		for delta := now - (*p).time; delta > expireDelta; delta = now - (*p).time {
			atomic.AddUint64(counter, ^uint64(0))
			*p = (*p).prev
			if *p == nil {
				break
			}
		}
	}
}

func (c *requestCounter) checkExpires(now int64, expireDelta int64, counters ...*uint64) {
	if c.tail == nil {
		c.flushCounters(counters...)
		return
	}
	p := c.tail
	for delta := now - p.time; delta > expireDelta; delta = now - p.time {
		for _, counter := range counters {
			atomic.AddUint64(counter, ^uint64(0))
		}
		c.removeLast()
		p = c.tail
		if c.head == nil {
			break
		}
	}
}

func (c *requestCounter) flushCounters(counters ...*uint64) {
	for _, counter := range counters {
		atomic.StoreUint64(counter, 0)
	}
}

func (c *requestCounter) incCounters(counters ...*uint64) {
	for _, counter := range counters {
		atomic.AddUint64(counter, 1)
	}
}

func (c *requestCounter) flush() {
	c.mutex.Lock()
	c.head = nil
	c.tail = nil
	c.minute = nil
	c.second = nil
	c.mutex.Unlock()
	c.flushCounters(&c.perHour, &c.perMinute, &c.perSecond)
}

func (c *requestCounter) removeLast() {
	t := c.tail.prev
	if t != nil {
		t.next = nil
	}
	if c.minute == c.tail {
		c.minute = t
	}
	if c.second == c.tail {
		c.second = t
	}
	c.tail = t

	if c.tail == nil {
		c.head = nil
	}
}

func (c *requestCounter) getRPH() uint64 {
	c.refresh(time.Now().UnixNano())
	return atomic.LoadUint64(&c.perHour)
}

func (c *requestCounter) getRPM() uint64 {
	c.refresh(time.Now().UnixNano())
	return atomic.LoadUint64(&c.perMinute)
}

func (c *requestCounter) getRPS() uint64 {
	c.refresh(time.Now().UnixNano())
	return atomic.LoadUint64(&c.perSecond)
}

func (c *requestCounter) getTotal() uint64 {
	return atomic.LoadUint64(&c.total)
}
